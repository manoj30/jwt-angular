const Users = require('../models/users.model.js');
const Chat = require('../models/chat.model.js');
const Friends = require('../models/friends.model.js');
exports.create = (req, res) => { // SIGNUP
    let postData = req.body;
    let newUser = new Users();
    newUser.fullname = postData.fullname,
        newUser.username = postData.username,
        newUser.email = postData.email
    newUser.setPassword(postData.matching_passwords.password);
    newUser.save((err, data) => {
        if (err) {
            return res.json({ status: 500, message: "Failed to create account.", data: err });
        }
        else {
            return res.json({ status: 200, message: "Account created succesfully.", data: data });
        }
    });
};
exports.doSignin = (req, res) => { // DO SIGNIN
    Users.findOne({ email: req.body.email }, function (err, data) {
        if (data === null) {
            return res.json({ status: 400, message: 'Email not found', data: err });
        }
        else {
            if (data.validPassword(req.body.password)) {
                const token = data.generateAuthToken();
                return res.json({ status: 200, message: 'loggedin Successfully', data: data, token: token });
            }
            else {
                return res.json({ status: 400, message: 'Wrong Password', data: data });
            }
        }
    });
};
exports.getUsername = (req, res) => { // GET THE ALL USERS
    Users.find(function (err, data) {
        if (data === null) {
            return res.json({ status: 400, message: 'Users not found!', data: err });
        }
        else {
            return res.json({ status: 400, message: 'Users found!', data: data });
        }
    });
}

exports.getAllUsers = (req, res) => { // SIGNUP
    let postData = req.body;
    Users.find({ _id: {$ne: postData.id } }, (err, data) => {
        if (err) {
            return res.json({ status: 500, message: "Users not found.", data: err });
        }
        else {
            return res.json({ status: 200, message: "Get all users successfully.", data: data });
        }
    });
};
exports.getFriends = (req, res) => { // SIGNUP
    let postData = req.body;
    Friends.find({ userID: postData.id }, (err, data) => {
        if (err) {
            return res.json({ status: 500, message: "Friends not found.", data: err });
        }
        else {
            return res.json({ status: 200, message: "Get all friends successfully.", data: data });
        }
    });
};
exports.saveMessage = (req, res) => { // SIGNUP
    let postData = req.body;
    let chat = new Chat();
    chat.fullname = 'manoj',
        chat.username = 'mkpatidar',
        chat.save((err, data) => {
            if (err) {
                return res.json({ status: 500, message: "Failed to create account.", data: err });
            }
            else {
                return res.json({ status: 200, message: "Account created successfully.", data: data });
            }
        });
};