var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const jwt = require('jsonwebtoken');

var app = express();
app.use(function (req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1]
    jwt.verify(token, key.tokenKey, function (err, payload) {
      console.log("payload ====== ", payload);
      if (payload) {
        /* user.findById(payload.userId).then(
          (doc)=>{
            req.user=doc;
            next()
          }
        ) */
      } else {
        next()
      }
    })
  } catch (e) {
    next()
  }
});

// MONGODB CONNECTION
/* var MongoClient = require('mongodb').MongoClient
MongoClient.connect('mongodb://localhost:27017/jwtAngular', function (err, db) {
  if (err) throw err
  console.log("Database connected successfully!");
}) */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/jwtAngular', { useNewUrlParser: true })

// view engine setup
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'jade');
app.use(cors(
  { origin: 'http://localhost:4200' }
));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
// app.use('/users/doSignup', usersRouter);
// app.use('/users/getUsername', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// SOCKET
const http = require('http').Server(app);
const io = require('socket.io')(http);

const documents = [];
// import controller file
const Chat = require('./models/chat.model.js');
const Users = require('./models/users.model.js');
const Friends = require('./models/friends.model.js');
io.on("connection", socket => {
  
  let previousId;
  const safeJoin = currentId => {
    socket.leave(previousId);
    socket.join(currentId);
    previousId = currentId;
  };
  
  // GET USER'S CHAT HISTRY
  socket.on("getDoc", iDs => {
    let whereClouse = { 
      $or: [ 
        { userID: iDs.userID, partnerID: iDs.partnerID},  
        { userID: iDs.partnerID, partnerID: iDs.userID }  
      ]  
    }
    Chat.find(whereClouse, (err, user) => {
      if(user && user.length){
        let docArray = []
        for(let obj of user){
          docArray.push(obj);
        }
        // safeJoin(iDs.userID);
        safeJoin(iDs.chatID);
        socket.emit("document", docArray);
      }else{
        socket.emit("document", []);
      }
    });
  });
  
  // SAVE CHAT
  socket.on("editDoc", doc => {
    let chat = new Chat();
    chat.userID = doc.userID;
    chat.partnerID = doc.partnerID;
    chat.message = doc.message;
    chat.save((err, savedData) => {
      doc.allChats.push(savedData)
      if(!err){
        socket.to(doc.chatID).emit("document", doc.allChats);
        socket.emit("document", doc.allChats);
      }
    });
  });

  // ADD FREINDS
  socket.on("addDoc", postData => {
    let userRow = new Friends();
    userRow.userID = postData.userID;  
    userRow.partnerID = postData.partnerID;
    userRow.partnerName = postData.partnerName;
    userRow.status = 'accept';
    userRow.chatID = postData.chatID;
    userRow.save((err, savedData) => {  // SAVED THE ROW FOR USER
      postData.documents.push(savedData)
      if(!err){
        // safeJoin(savedData.chatID);
        // socket.emit("document", savedData);
      }
    });
    
    let partnerRow = new Friends();
    partnerRow.userID = postData.partnerID;  
    partnerRow.partnerID = postData.userID;  
    partnerRow.partnerName = postData.userName;
    partnerRow.status = 'accept';
    partnerRow.chatID = postData.chatID;
    partnerRow.save((err, savedData) => { } ); // SAVED THE EXTRA ROW FOR PARTNER 
  });

  // io.emit("documents", documents);

  
  
  
  /* socket.on("addDoc", doc => {
    documents[doc.id] = doc;
    safeJoin(doc.id);
    io.emit("documents", Object.keys(documents));
    socket.emit("document", doc);
  }); */
  
  /* socket.on("getDoc", docId => {
    safeJoin(docId);
    socket.emit("document", documents[docId]);
  }); */

  /* socket.on("editDoc", doc => {
    documents[doc.id] = doc;
    socket.to(doc.id).emit("document", doc);
  }); */

  // io.emit("documents", documents);
});

http.listen(4444);

module.exports = app;
