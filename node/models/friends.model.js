const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    userID: String,
    partnerID: String,
    partnerName: String,
    status: String,
    chatID: String
}, {
    timestamps: true
});

module.exports = mongoose.model('friends', UsersSchema);