const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    userID: String,
    partnerID: String,
    message: String,
    status: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('chat', UsersSchema);