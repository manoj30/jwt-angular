import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AlertService } from './../../core/alert/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DocumentService } from './../document/document.service';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy {
  documents: Observable<string[]>;
  allUsers: any = [];
  allFriends: any = [];
  private _docSub: Subscription;

  constructor(
    private documentService: DocumentService,
    private toastr: ToastrService,
    private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.documents = this.documentService.documents;
    this._docSub = this.documentService.currentDocument.subscribe(doc => {} );
    this.getFriends();
  }

  ngOnDestroy() {
    this._docSub.unsubscribe();
  }

  loadDoc(obj: any) {
    this.documentService.sendActionChildToParent(obj);
    this.documentService.getDocument(obj);
  }

  newDoc(obj) {
    this.documentService.newDocument(obj, this.documents);
    window.location.reload();
  }








  // ====================================================================================
  getAllUsers() {
    this.documentService.getAllUsers().subscribe((data) => {
      if (data.status === 200) {
        let tempValue = [];
        data.data.forEach(obj=> {
          let found = this.allFriends.filter(function(e){ return e.partnerID == obj._id });
          if(found && found.length){
            obj['added'] = true;
          }
          tempValue.push(obj);
        })
        this.allUsers = tempValue;
      }
    }, (error) => {
      this.toastr.error('There are some server Please check connection.', 'Error');
    });
  }
  getFriends() {
    this.documentService.getFriends().subscribe((data) => {
      if (data.status === 200) {
        this.allFriends = data.data;
        this.getAllUsers();
      }
    }, (error) => {
      this.toastr.error('There are some server Please check connection.', 'Error');
    });
  }

}