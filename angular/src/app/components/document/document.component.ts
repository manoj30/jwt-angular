import { Component, OnInit, OnDestroy } from '@angular/core';
import { DocumentService } from './document.service';
import { Subscription } from 'rxjs';
import { Document } from './document.model';
import { startWith } from 'rxjs/operators';
import { JwtService } from './../../core/jwt.service';
import { currentUser } from './../header/header.model';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit, OnDestroy {
  document: Document = new Document;
  allChats: [];
  partner: any = {};
  private _docSub: Subscription;
  subscription: Subscription;
  loggedInUser: currentUser = new currentUser;
  constructor(
    private jwtService: JwtService,
    private documentService: DocumentService
  ) {
    this.loggedInUser = JSON.parse(this.jwtService.getCurrentUser());
    this.subscription = this.documentService.getActionChildToParent().subscribe(partnerObj => {
      if (partnerObj) {
        this.partner = partnerObj;
      }
    });
  }

  ngOnInit() {
    this._docSub = this.documentService.currentDocument.subscribe(document =>     
      // console.log("========= this.allChats document",document)
      this.allChats = document 
    );
  }

  ngOnDestroy() {
    this._docSub.unsubscribe();
  }

  editDoc() {
    console.log('this.partner ---- ',this.partner);
    if(!this.document.message){
      alert("Please type your message first!");
      return;
    }
    if(this.partner && this.partner.partnerID){
      let data = {
        document: this.document,
        userID: this.loggedInUser.id,
        partnerID: this.partner.partnerID,
        chatID: this.partner.chatID,
        message: this.document.message
      }
      console.log('data ---- ',data);
      if(this.allChats){
        data['allChats'] = this.allChats;
      }else{
        data['allChats'] = [];
      }
      this.documentService.editDocument(data);
    }else{
      alert("Please select your friend first!");
    }
    this.document = new Document;
  }
}