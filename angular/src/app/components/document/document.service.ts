import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Document } from './document.model';
import { Observable, Subject } from 'rxjs';
import { JwtService } from './../../core/jwt.service';
import { currentUser } from './../header/header.model';
import { ApiService } from './../api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  currentDocument = this.socket.fromEvent<any>('document');
  documents: any = this.socket.fromEvent<string[]>('documents');
  loggedInUser: currentUser = new currentUser;

  constructor(
    private socket: Socket,
    private jwtService: JwtService,
    private apiService: ApiService
  ) { 
    this.loggedInUser = JSON.parse(this.jwtService.getCurrentUser());
  }

  private subject = new Subject<any>();
  getActionChildToParent(): Observable<any> {
    return this.subject.asObservable();
  }
  sendActionChildToParent(partnerObj: any) {
    this.subject.next(partnerObj);
  }

  getDocument(obj: any) {
    let iDs = {
      userID: this.loggedInUser.id,
      partnerID: obj.partnerID,
      chatID: obj.chatID
    }
    this.socket.emit('getDoc', iDs);
  }

  newDocument(partner, documents) {
    let postObj = {
      userID: this.loggedInUser.id,
      userName: this.loggedInUser.name,
      partnerName: partner.fullname,
      partnerID: partner._id,
      chatID: this.docId(),
      documents: []
    }
    if(documents && documents.length){
      postObj.documents = documents;
    }
    this.socket.emit('addDoc', postObj);
  }

  editDocument(document: any) {
    this.socket.emit('editDoc', document);
  }

  private docId() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }




  // =========================================================================
  public getAllUsers(): any {
    return this.apiService.post('users/getAllUsers', this.loggedInUser).pipe(map(
      data => {
        return data
      }
    ));
  }
  public getFriends(): any {
    return this.apiService.post('users/getFriends', this.loggedInUser).pipe(map(
      data => {
        return data
      }
    ));
  }

}