export class Document {
	_id: string;
	userID: string;
	partnerID: string;
	message: string;
	status: string;
}